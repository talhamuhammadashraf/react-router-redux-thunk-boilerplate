import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
const test = (state = 0, action) => {
  switch (action.type) {
    case "one":
      return state + 1;
    default:
      return state;
  }
};
export default history =>
  combineReducers({
    router: connectRouter(history),
    test
  });
